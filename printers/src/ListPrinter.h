//
// Created by Amadeus on 2022/9/14.
//

#ifndef EULER_PROJECT_CPP_LISTPRINTER_H
#define EULER_PROJECT_CPP_LISTPRINTER_H

#include <string>
#include "struct.h"

class ListPrinter {
public:
    static std::string print(ListNode *head);
};


#endif //EULER_PROJECT_CPP_LISTPRINTER_H
