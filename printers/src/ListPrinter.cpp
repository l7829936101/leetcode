//
// Created by Amadeus on 2022/9/14.
//

#include "ListPrinter.h"

std::string ListPrinter::print(ListNode *head) {
    if (head == nullptr) {
        return "[]";
    }
    std::string res = "[";
    res += std::to_string(head->val);
    head = head->next;
    while (head) {
        res += std::to_string(head->val) + ", ";
        head = head->next;
    }
    res += ']';
    return res;
}
