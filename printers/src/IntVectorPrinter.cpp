//
// Created by Amadeus on 2022/9/14.
//

#include "IntVectorPrinter.h"

std::string IntVectorPrinter::print(const std::vector<int> &nums) {
    if (nums.empty()) {
        return "[]";
    }
    std::string res = "[";
    res += std::to_string(nums[0]);
    for (int i = 1; i < nums.size(); ++i) {
        res += ", " + std::to_string(nums[i]);
    }
    res += ']';
    return res;
}

std::ostream &operator<<(std::ostream &os, const std::vector<int> &nums) {
    os << IntVectorPrinter::print(nums);
    return os;
}