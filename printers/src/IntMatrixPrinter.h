//
// Created by Amadeus on 2022/9/14.
//

#ifndef EULER_PROJECT_CPP_INTMATRIXPRINTER_H
#define EULER_PROJECT_CPP_INTMATRIXPRINTER_H

#include <string>
#include <vector>
#include <ostream>
#include "IntVectorPrinter.h"

class IntMatrixPrinter {
public:
    static std::string print(const std::vector<std::vector<int>> &mat);
};

std::ostream &operator<<(std::ostream &os, const std::vector<std::vector<int>> &mat);

#endif //EULER_PROJECT_CPP_INTMATRIXPRINTER_H
