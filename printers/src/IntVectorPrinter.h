//
// Created by Amadeus on 2022/9/14.
//

#ifndef EULER_PROJECT_CPP_INTVECTORPRINTER_H
#define EULER_PROJECT_CPP_INTVECTORPRINTER_H

#include <string>
#include <vector>
#include <ostream>

class IntVectorPrinter {
public:
    static std::string print(const std::vector<int> &nums);
};

std::ostream &operator<<(std::ostream &os, const std::vector<int> &nums);

#endif //EULER_PROJECT_CPP_INTVECTORPRINTER_H
