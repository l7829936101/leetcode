//
// Created by Amadeus on 2022/9/14.
//

#include "BinaryTreePrinter.h"


static void print(std::string &res, const std::string &prefix, const TreeNode *node, bool is_left) {
    if (nullptr == node) {
        return;
    }
    res += prefix;
    res += is_left ? "├──" : "└──";
    res += std::to_string(node->val) + '\n';
    print(res, prefix + (is_left ? "│  " : "   "), node->left, true);
    print(res, prefix + (is_left ? "│  " : "   "), node->right, false);

}

std::string BinaryTreePrinter::print(const TreeNode *root) {
    std::string res;
    ::print(res, "", root, false);
    return res;
}

std::ostream &operator<<(std::ostream &os, const TreeNode *root) {
    os << BinaryTreePrinter::print(root);
    return os;
}
