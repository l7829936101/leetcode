//
// Created by Amadeus on 2022/9/14.
//

#ifndef EULER_PROJECT_CPP_BINARYTREEPRINTER_H
#define EULER_PROJECT_CPP_BINARYTREEPRINTER_H

#include <string>
#include <ostream>
#include "struct.h"

class BinaryTreePrinter {
public:
    static std::string print(const TreeNode *root);
};

std::ostream &operator<<(std::ostream &os, const TreeNode *root);

#endif //EULER_PROJECT_CPP_BINARYTREEPRINTER_H
