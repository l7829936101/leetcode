//
// Created by Amadeus on 2022/9/14.
//

#include "IntMatrixPrinter.h"

std::string IntMatrixPrinter::print(const std::vector<std::vector<int>> &mat) {
    if (mat.empty()) {
        return "[]";
    }
    std::string res = "[";
    res += IntVectorPrinter::print(mat[0]);
    for (int i = 1; i < mat.size(); ++i) {
        res += ",\n" + IntVectorPrinter::print(mat[i]);
    }
    res += ']';
    return res;
}

std::ostream &operator<<(std::ostream &os, const std::vector<std::vector<int>> &mat) {
    os << IntMatrixPrinter::print(mat);
    return os;
}