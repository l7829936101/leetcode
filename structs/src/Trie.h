//
// Created by Amadeus on 3/24/23.
//

#ifndef ALOGRITHMC_TRIE_H
#define ALOGRITHMC_TRIE_H

#include <vector>
#include <string>

struct TrieNode {
    bool isWord;
    std::vector<TrieNode *> children;

    TrieNode() : isWord(false), children(26, nullptr) {}
};


class Trie {
private:
    TrieNode *root{};

public:
    Trie() : root(new TrieNode()) {}

    explicit Trie(std::vector<std::string> &words);

    bool search(std::string &word);

    bool prefix(std::string &word);

    void extend(std::string &word);
};


#endif //ALOGRITHMC_TRIE_H
