//
// Created by Amadeus on 12/16/22.
//

#ifndef ALOGRITHMC_VECTORHASHER_H
#define ALOGRITHMC_VECTORHASHER_H

#include <vector>

template<typename T>
class VectorHasher {
public:
    std::size_t operator()(const std::vector<T> &vec) const {
        std::size_t seed = vec.size();
        for (auto &i: vec) {
            seed ^= i + 0x9e3779b9 + (seed << 6) + (seed >> 2);
        }
        return seed;
    }
};

#endif //ALOGRITHMC_VECTORHASHER_H
