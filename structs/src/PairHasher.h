//
// Created by Amadeus on 12/16/22.
//

#ifndef ALOGRITHMC_PAIRHASHER_H
#define ALOGRITHMC_PAIRHASHER_H

#include <utility>

template<typename T1, typename T2>
class PairHasher {
public:
    std::size_t operator()(const std::pair<T1, T2> &pair) const {
        std::size_t seed = 0;
        seed ^= pair.first + 0x9e3779b9 + (seed << 6) + (seed >> 2);
        seed ^= pair.second + 0x9e3779b9 + (seed << 6) + (seed >> 2);
        return seed;
    }
};

#endif //ALOGRITHMC_PAIRHASHER_H
