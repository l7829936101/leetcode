//
// Created by Amadeus on 2022/10/25.
//

#ifndef ALOGRITHMC_MERGESET_H
#define ALOGRITHMC_MERGESET_H

#include <vector>
#include <unordered_set>
#include <unordered_map>
#include <numeric>

class MergeSet {
    std::vector<int> root;

    inline int _find_root(int id) {
        std::vector<int> ids;
        while (root[id] != id) {
            ids.push_back(id);
            id = root[id];
        }
        for (auto i: ids) {
            root[i] = id;
        }
        return id;
    }

    /*
     * @param id : the index of node to query
     * @return: tree length
     * @effect:
     *      The id is set to root node's index
     *      Path is compressed
     * */
    inline int move_to_root(int &id) {
        std::vector<int> ids;
        while (root[id] != id) {
            ids.push_back(id);
            id = root[id];
        }
        for (auto i: ids) {
            root[i] = id;
        }
        return (int) ids.size() + 1;
    }

public:
    explicit MergeSet(int n) {
        root.resize(n);
        std::iota(root.begin(), root.end(), 0);
    }

    inline int merge(int id1, int id2) {
        int l1 = move_to_root(id1);
        int l2 = move_to_root(id2);
        if (l1 <= l2) {
            root[id1] = root[id2];
            return root[id2];
        } else {
            root[id2] = root[id1];
            return root[id1];
        }
    }

    inline int query(int idx) {
        return _find_root(idx);
    }

    inline void compress() {
        int n = (int) root.size();
        for (int i = 0; i < n; ++i) {
            _find_root(i);
        }
    }

    inline int count() {
        compress();
        return std::unordered_set<int>(root.begin(), root.end()).size();
    }

    inline std::vector<std::vector<int>> get_clusters(bool remove_single = false, bool sorted = false) {
        compress();
        std::vector<std::vector<int>> ans;
        std::unordered_map<int, int> clusters;
        for (int i = 0; i < root.size(); ++i) {
            if (!clusters.count(root[i])) {
                clusters.emplace(root[i], ans.size());
                ans.emplace_back();
            }
            ans[clusters[root[i]]].push_back(i);
        }
        if (remove_single) {
            ans.erase(std::remove_if(ans.begin(), ans.end(), [](const auto &c) { return c.size() == 1; }), ans.end());
        }
        if (sorted) {
            std::sort(ans.begin(), ans.end(), [](const auto &c1, const auto &c2) {
                return c1.size() < c2.size();
            });
        }
        return ans;
    }

};

#endif //ALOGRITHMC_MERGESET_H
