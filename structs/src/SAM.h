//
// Created by Amadeus on 2022/10/29.
//

#ifndef ALOGRITHMC_SAM_H
#define ALOGRITHMC_SAM_H

#include <map>
#include <string>

struct State {
public:
    int len = 0;
    bool is_suffix = false;
    std::map<char, std::shared_ptr<State>> next;
    State *back = nullptr;

    ~State();
};


class SAM {

    inline void extend(char c, State *&last) const;

    inline void mark(State *&last) const;

public:
    std::shared_ptr<State> init = std::make_shared<State>();


    explicit SAM(const std::string &s);


    [[nodiscard]] bool is_substr(const std::string &s) const;

    ~SAM() {
        init.reset();
    }
};

#endif //ALOGRITHMC_SAM_H
