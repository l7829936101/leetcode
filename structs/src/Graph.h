//
// Created by Amadeus on 12/18/22.
//

#ifndef ALOGRITHMC_GRAPH_H
#define ALOGRITHMC_GRAPH_H

#include <vector>
#include <utility>
#include <queue>
#include <stack>
#include <map>

struct Graph {
    const int n = 0;

    explicit Graph(int size) : n(size) {};
};

struct ATGraph : Graph {

    std::vector<std::vector<int>> adjTab;

    explicit ATGraph(int size);

    ATGraph(int size, const std::vector<std::vector<int>> &edgeList);

    void add_edge(int source, int target, bool biDirect);
};

struct AMGraph : Graph {
    std::vector<std::vector<int>> adjMat;

    explicit AMGraph(int size);

    AMGraph(int size, const std::vector<std::vector<int>> &edgeList);
};

struct ATWGraph : Graph {
    std::vector<std::vector<std::pair<int, int>>>
            adjTab;

    explicit ATWGraph(int size);

    ATWGraph(int size, const std::vector<std::vector<int>> &edgeList);

    void add_edge(int source, int target, int weight, bool biDirect);
};

class Dijkstra {
public:
    static std::vector<int> find(int source, int target, ATWGraph &G);
};

class SPFA {
public:
    static inline bool circ_check(int cur, int source, std::vector<std::pair<int, int>> &pre) {
        if (pre[source].first != source) {
            return true;
        }
        std::vector<bool> visited(pre.size(), false);
        while (cur != source) {
            visited[cur] = true;
            cur = pre[cur].first;
            if (visited[cur]) {
                return true;
            }
        }
        return false;
    }

public:
    static std::vector<int> find(int source, int target, ATWGraph &G, bool check_neg = true);
};

class KPath {

    static std::vector<std::vector<int>> quick_pow(const std::vector<std::vector<int>> &mat, int k);

public:
    static int find(int source, int target, int k, AMGraph &G);
};

class BiGraph {
    static bool draw(int cur, int c, std::vector<int> color, ATGraph &G);

public:
    static bool check(ATGraph &G);
};

class TurboSort {
public:
    static std::vector<int> sort(ATGraph &G);
};

class Tarjan {
    std::vector<int> dfn;
    std::vector<int> low;
    std::stack<int> stk;
    std::vector<int> visit;

    void dfs(int cur, int &nth, ATGraph &G, std::vector<std::vector<int>> &ans);

public:
    std::vector<std::vector<int>> find(ATGraph &G);
};

class Kosaraju {

    std::vector<bool> visited;

    std::stack<bool> order;

    void get_component(int cur, ATGraph &G, std::vector<int> &comp);

    static ATGraph reverse_graph(ATGraph &G);

public:
    std::vector<std::vector<int>> find(ATGraph &G);

    void get_order(int cur, ATGraph &G);
};

#endif //ALOGRITHMC_GRAPH_H
