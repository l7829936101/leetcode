//
// Created by Amadeus on 2022/10/30.
//
#include <vector>
#include "SAM.h"

State::~State() {
    for (auto &i: next) {
        i.second.reset();
    }
}

inline void SAM::extend(char c, State *&last) const {
    // 新字符引入导致产生新的结束位置，因此对新引入后缀进行处理
    auto np = std::make_shared<State>();
    np->len = last->len + 1;
    // 从 last 状态开始，沿着后缀链接遍历，添加新的子串
    auto p = last;
    while (p != nullptr) {
        if (p->next.find(c) == p->next.end()) {
            // 若没有到 c 的转移，就增加一个到当前状态的转移
            p->next.emplace(c, np);
        } else {
            // 否则退出遍历
            break;
        }
        p = p->back;
    }
    if (p == nullptr) {
        // 新引入的一系列后缀此前没有出现过，处理完毕
        np->back = init.get();
    } else {
        // 记状态 p 通过字符 c 转移到 q，即新后缀此前出现过
        auto q = p->next[c].get();
        if (p->len + 1 == q->len) {
            // 若 p 的长度加一等于 q 的长度，说明 q 的后缀链接已经是最短的了，不需要处理
            np->back = q;
        } else {
            // 否则，q当中的一部分串的结束位置增加了，导致 q 的分裂。
            auto nq = std::make_shared<State>(*q);
            nq->len = p->len + 1;
            q->back = nq.get();
            np->back = nq.get();
            // 从 p 开始，沿着后缀链接遍历，修改所有到 q 的转移为到 nq
            while (p != nullptr && p->next[c].get() == q) {
                p->next[c] = nq;
                p = p->back;
            }
        }
    }
    // 更新 last
    last = np.get();
}


inline void SAM::mark(State *&last) const {
    // 从后往前遍历，标记所有的后缀
    while (last) {
        last->is_suffix = true;
        last = last->back;
    }
    init->is_suffix = false;
}

SAM::SAM(const std::string &s) {
    State *last = init.get();
    for (auto c: s) {
        extend(c, last);
    }
    mark(last);
}


bool SAM::is_substr(const std::string &s) const {
    auto p = init.get();
    for (auto c: s) {
        if (p->next.find(c) == p->next.end()) {
            return false;
        }
        p = p->next[c].get();
    }
    return p->is_suffix;
}