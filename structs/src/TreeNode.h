//
// Created by Amadeus on 2022/9/14.
//

#ifndef EULER_PROJECT_CPP_TREENODE_H
#define EULER_PROJECT_CPP_TREENODE_H

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;

    TreeNode() : val(0), left(nullptr), right(nullptr) {}

    explicit TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}

    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

#endif //EULER_PROJECT_CPP_TREENODE_H
