//
// Created by Amadeus on 2022/9/14.
//

#ifndef EULER_PROJECT_CPP_LISTNODE_H
#define EULER_PROJECT_CPP_LISTNODE_H

struct ListNode {
    int val;
    ListNode *next;

    ListNode() : val(0), next(nullptr) {}

    explicit ListNode(int x) : val(x), next(nullptr) {}

    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

#endif //EULER_PROJECT_CPP_LISTNODE_H
