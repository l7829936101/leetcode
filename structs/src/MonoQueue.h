//
// Created by Amadeus on 2022/10/24.
//

#ifndef ALOGRITHMC_MONOQUEUE_H
#define ALOGRITHMC_MONOQUEUE_H

#include <deque>
#include <queue>

template<typename T, class Compare>
class MonoQueue {
    std::queue<T> content;
    std::deque<T> prior;
    Compare comp;
public:

    inline bool empty() {
        return content.empty();
    }

    inline void push(T t) {
        while (!prior.empty() && !comp(t, prior.back())) {
            prior.pop_back();
        }
        prior.push_back(t);
        content.template emplace(std::forward<T &&>(t));
    }

    template<class ...Args>
    inline
#if _LIBCPP_STD_VER > 14
    typename std::queue<T>::reference
#else
    void
#endif
    emplace(Args &&... args) {
        T &&t = T(std::forward<Args &&...>(args)...);
        while (!prior.empty() && !comp(t, prior.back())) {
            prior.pop_back();
        }
        prior.push_back(t);
        content.template emplace(std::forward<T &&>(t));
#if _LIBCPP_STD_VER > 14
        return content.back();
#endif
    }

    inline T &front() {
        T &t = content.front();
        return t;
    }

    inline T &query() {
        return prior.front();
    }

    void pop() {
        T &res = content.front();
        content.pop();
        if (comp(prior.front(), res)) {
            prior.pop_front();
        }
    }

    void swap(MonoQueue<T, Compare> other) {
        std::swap(prior, other.prior);
        std::swap(content, other.content);
    }
};

template<typename T>
class MinQueue : public MonoQueue<T, std::greater_equal<>> {
};

template<typename T>
class MaxQueue : public MonoQueue<T, std::less_equal<>> {
};

template<typename T>
class MinMaxQueue {
    std::queue<T> content;
    std::deque<T> minQ;
    std::deque<T> maxQ;

public:

    inline bool empty() {
        return content.empty();
    }

    inline void push(T t) {
        while (!minQ.empty() && t < minQ.back()) {
            minQ.pop_back();
        }
        minQ.push_back(t);
        while (!maxQ.empty() && t > maxQ.back()) {
            maxQ.pop_back();
        }
        maxQ.push_back(t);
        content.template emplace(std::forward<T &&>(t));
    }

    template<class ...Args>
    inline
#if _LIBCPP_STD_VER > 14
    typename std::queue<T>::reference
#else
    void
#endif
    emplace(Args &&... args) {
        T &&t = T(std::forward<Args &&...>(args)...);
        while (!minQ.empty() && t < minQ.back()) {
            minQ.pop_back();
        }
        minQ.push_back(t);
        while (!maxQ.empty() && t > maxQ.back()) {
            maxQ.pop_back();
        }
        maxQ.push_back(t);
        content.template emplace(std::forward<T &&>(t));
#if _LIBCPP_STD_VER > 14
        return content.back();
#endif
    }

    inline T &front() {
        T &t = content.front();
        return t;
    }

    inline T &min() {
        return minQ.front();
    }

    inline T &max() {
        return maxQ.front();
    }

    void pop() {
        T &res = content.front();
        content.pop();
        if (minQ.front() == res) {
            minQ.pop_front();
        }
        if (maxQ.front() == res) {
            maxQ.pop_front();
        }
    }

    void swap(MinMaxQueue<T> other) {
        std::swap(content, other.content);
        std::swap(minQ, other.minQ);
        std::swap(maxQ, other.maxQ);
    }
};

#endif //ALOGRITHMC_MONOQUEUE_H
