//
// Created by Amadeus on 3/24/23.
//

#include <queue>
#include "ACAutomaton.h"

ACAutomaton::ACAutomaton(std::vector<std::string> &words) {
    // 构建前缀树
    root = new ACTrieNode();
    for (auto &word: words) {
        auto *node = root;
        for (auto &c: word) {
            if (node->children[c - 'a'] == nullptr) {
                node->children[c - 'a'] = new ACTrieNode();
            }
            node = node->children[c - 'a'];
        }
        node->has_word_suffix = true;
    }
    // 寻找失配指针
    root->fail = root;
    std::queue<ACTrieNode *> q;
    for (auto node: root->children) {
        if (node) {
            node->fail = root;
            q.push(node);
        }
    }
    while (!q.empty()) {
        auto node = q.front();
        q.pop();
        node->has_word_suffix = node->has_word_suffix || node->fail->has_word_suffix; // 优化，避免在失配路径上检测
        for (int i = 0; i < 26; i++) {
            if (node->children[i]) {
                auto fail = node->fail;
                while (fail != root && fail->children[i] == nullptr) {
                    fail = fail->fail;
                }
                if (fail->children[i]) {
                    fail = fail->children[i];
                }
                node->children[i]->fail = fail;
                q.push(node->children[i]);
            } else {
                // 路径压缩优化，避免在失配路径上搜索
                node->children[i] = node->fail->children[i];
            }
        }
    }
}

/*
 * @param s：用于比较的主串
 * @return：主串中是否存在一个后缀匹配某个模式
 */
bool ACAutomaton::suffix(std::string &s) {
    auto *node = root;
    for (auto &c: s) {
        while (node != root && node->children[c - 'a'] == nullptr) {
            node = node->fail;
        }
        if (node->children[c - 'a']) {
            node = node->children[c - 'a'];
        }
    }
    return node->has_word_suffix;
}