//
// Created by Amadeus on 3/24/23.
//

#include "Trie.h"

Trie::Trie(std::vector<std::string> &words) {
    // build tire tree according to word in words
    root = new TrieNode();
    for (auto &word: words) {
        auto *node = root;
        for (auto &c: word) {
            if (node->children[c - 'a'] == nullptr) {
                node->children[c - 'a'] = new TrieNode();
            }
            node = node->children[c - 'a'];
        }
        node->isWord = true;
    }
}

bool Trie::search(std::string &word) {
    auto *node = root;
    for (auto &c: word) {
        if (node->children[c - 'a'] == nullptr) {
            return false;
        }
        node = node->children[c - 'a'];
    }
    return node->isWord;
}

bool Trie::prefix(std::string &word) {
    auto *node = root;
    for (auto &c: word) {
        if (node->isWord) {
            return true;
        }
        if (node->children[c - 'a'] == nullptr) {
            return false;
        }
        node = node->children[c - 'a'];
    }
    return node->isWord;
}

void Trie::extend(std::string &word) {
    auto *node = root;
    for (auto &c: word) {
        if (node->children[c - 'a'] == nullptr) {
            node->children[c - 'a'] = new TrieNode();
        }
        node = node->children[c - 'a'];
    }
    node->isWord = true;
}
