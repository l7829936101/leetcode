//
// Created by Amadeus on 3/24/23.
//

#ifndef ALOGRITHMC_ACAUTOMATON_H
#define ALOGRITHMC_ACAUTOMATON_H

#include <vector>
#include <string>


struct ACTrieNode {
    bool has_word_suffix; // 存在某个后缀是模式
    std::vector<ACTrieNode *> children;
    ACTrieNode *fail{}; // 失配指针，指向自动机当中，当前串的最长真子串

    ACTrieNode() : has_word_suffix(false), children(26, nullptr) {}
};

class ACAutomaton {
private:
    ACTrieNode *root{};

public:
    explicit ACAutomaton(std::vector<std::string> &words);

    bool suffix(std::string &s);
};


#endif //ALOGRITHMC_ACAUTOMATON_H
