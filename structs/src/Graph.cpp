//
// Created by Amadeus on 12/18/22.
//

#include "Graph.h"

ATGraph::ATGraph(int size) : Graph(size) {}

ATGraph::ATGraph(int size, const std::vector<std::vector<int>> &edgeList) : Graph(size) {
    adjTab.resize(size);
    for (const auto &e: edgeList) {
        adjTab[e[0]].push_back(e[1]);
        adjTab[e[1]].push_back(e[0]);
    }
}

void ATGraph::add_edge(int source, int target, bool biDirect = false) {
    adjTab[source].push_back(target);
    if (biDirect) {
        adjTab[target].push_back(source);
    }
}

AMGraph::AMGraph(int size) : Graph(size) {}

AMGraph::AMGraph(int size, const std::vector<std::vector<int>> &edgeList)
        : Graph(size), adjMat(std::vector<std::vector<int >>(n, std::vector<int>(n, 0))) {
    for (const auto &e: edgeList) {
        adjMat[e[0]][e[1]] = 1;
    }
}

ATWGraph::ATWGraph(int size, const std::vector<std::vector<int>> &edgeList) : Graph(size) {
    adjTab.resize(size);
    for (const auto &e: edgeList) {
        adjTab[e[0]].emplace_back(e[1], e[2]);
        adjTab[e[1]].emplace_back(e[0], e[2]);
    }
}

ATWGraph::ATWGraph(int size) : Graph(size) {}

void ATWGraph::add_edge(int source, int target, int weight, bool biDirect = false) {
    adjTab[source].emplace_back(target, weight);
    if (biDirect) {
        adjTab[target].emplace_back(source, weight);
    }
}

std::vector<int> Dijkstra::find(int source, int target, ATWGraph &G) {
    std::priority_queue<std::pair<int, int>> distance;
    for (int i = 0; i < G.n; ++i) {
        distance.emplace(i, INT32_MAX);
    }
    distance.emplace(source, 0);

    std::vector<int> closed(G.n, false);

    std::vector<std::pair<int, int>> pre(G.n, {-1, INT_MAX});

    while (!distance.empty()) {
        // 取出最近节点
        int cur = distance.top().first;
        int dis = distance.top().second;
        distance.pop();

        if (closed[cur]) {
            continue;
        }

        if (dis == INT32_MAX) {
            break;
        }

        for (const auto &node: G.adjTab[cur]) {
            int next = node.first;
            int cost = dis + node.second;
            if (cost < pre[next].second) {
                pre[next].first = cur;
                pre[next].second = cost;
                distance.emplace(next, dis + node.second);
            }
        }
        closed[cur] = true;
    }

    // 回溯最短路径
    std::vector<int> path;
    int cur = target;
    while (cur != source) {
        path.push_back(cur);
        cur = pre[cur].first;
    }
    path.push_back(source);
    std::reverse(path.begin(), path.end());
    return path;
}

std::vector<int> SPFA::find(int source, int target, ATWGraph &G, bool check_neg) {

    // 松弛队列，表示节点开销有更新
    std::queue<int> relaxList;
    relaxList.push(source);
    // 标志是否正在松弛队列中，避免重复松弛
    std::vector<bool> relaxing(G.n, false);
    relaxing[source] = true;

    // 记录路径开销和前驱节点
    std::vector<std::pair<int, int>> pre(G.n, {-1, INT_MAX});
    pre[source] = {source, 0};

    while (!relaxList.empty()) {
        int cur = relaxList.front();
        relaxList.pop();
        relaxing[cur] = false;

        // 松弛 cur 节点
        for (auto node: G.adjTab[cur]) {
            int next = node.first;
            // 计算新开销
            int cost = pre[cur].second + node.second;
            if (pre[next].second > cost) {
                pre[next].first = cur;
                pre[next].second = cost;

                // 判断负权环
                if (check_neg && circ_check(next, source, pre)) {
                    return {};
                }

                // 加入松弛队列等待下一轮松弛
                if (!relaxing[next]) {
                    relaxList.push(next);
                    relaxing[next] = true;
                }
            }
        }
    }

    if (pre[target].second == INT_MAX) {
        return {};
    }

    // 回溯最短路径
    std::vector<int> path;
    int cur = target;
    while (cur != source) {
        path.push_back(cur);
        cur = pre[cur].first;
    }
    path.push_back(source);
    std::reverse(path.begin(), path.end());
    return path;
}

static void operator+=(std::vector<std::vector<int>> &mat1, const std::vector<std::vector<int>> &mat2) {
    const int n = (int) mat1.size();
    for (int i = 0; i < n; ++i)
        for (int j = 0; j < n; ++j)
            mat1[i][j] += mat2[i][j];
}

static void operator*=(std::vector<std::vector<int>> &mat1, std::vector<std::vector<int>> &mat2) {
    const int n = (int) mat1.size();
    std::vector<std::vector<int>> ans(n, std::vector<int>(n, 0));
    for (int i = 0; i < n; ++i)
        for (int j = 0; j < n; ++j)
            for (int k = 0; k < n; ++k)
                ans[i][j] += mat1[i][k] * mat2[k][j];
    mat1 = ans;
}

std::vector<std::vector<int>> KPath::quick_pow(const std::vector<std::vector<int>> &mat, int k) {
    const int n = (int) mat.size();
    std::vector<std::vector<int>> ans(n, std::vector<int>(n, 0));
    auto base = mat;
    while (k) {
        if (k & 1) {
            ans += base;
        }
        base *= base;
        k >>= 1;
    }
    return ans;
}

int KPath::find(int source, int target, int k, AMGraph &G) {
    const auto &mat = G.adjMat;
    auto ans = quick_pow(mat, k);
    return ans[source][target];
}

bool BiGraph::draw(int cur, int c, std::vector<int> color, ATGraph &G) {
    if (color[cur]) {
        return color[cur] == c;
    } else {
        color[cur] = c;
    }
    for (auto node: G.adjTab[cur]) {
        if (!draw(node, -c, color, G)) {
            return false;
        }
    }
    return true;
}

bool BiGraph::check(ATGraph &G) {
    std::vector<int> color(G.n, 0);
    for (int i = 0; i < G.n; ++i) {
        if (!color[i] && !draw(i, 1, color, G)) {
            return false;
        }
    }
    return true;
}

std::vector<int> TurboSort::sort(ATGraph &G) {
    std::vector<int> inDegree(G.n, 0);
    for (int i = 0; i < G.n; ++i) {
        for (auto node: G.adjTab[i]) {
            inDegree[node]++;
        }
    }

    std::queue<int> q;
    for (int i = 0; i < G.n; ++i) {
        if (inDegree[i] == 0) {
            q.push(i);
        }
    }

    std::vector<int> ans;
    while (!q.empty()) {
        int cur = q.front();
        q.pop();
        ans.push_back(cur);
        for (auto node: G.adjTab[cur]) {
            int next = node;
            inDegree[next]--;
            if (inDegree[next] == 0) {
                q.push(next);
            }
        }
    }
    return ans;
}

std::vector<std::vector<int>> Tarjan::find(ATGraph &G) {
    if (!G.n) {
        return {};
    }
    dfn.resize(G.n);
    low.resize(G.n, G.n);
    visit.resize(G.n, 0);
    int nth = 0;
    std::vector<std::vector<int>> ans;
    dfs(0, nth, G, ans);
    dfn.clear();
    low.clear();
    visit.clear();
    return ans;
}

void Tarjan::dfs(int cur, int &nth, ATGraph &G, std::vector<std::vector<int>> &ans) {
    visit[cur] = 1;
    low[cur] = dfn[cur] = nth++;

    for (auto node: G.adjTab[cur]) {
        if (!visit[cur]) {
            dfs(node, nth, G, ans);
            low[cur] = std::min(low[cur], low[node]);
        } else if (visit[cur] == 1) {
            low[cur] = std::min(low[cur], dfn[node]);
        }
    }

    visit[cur] = 2;
    if (low[cur] == dfn[cur]) {
        std::vector<int> component;
        while (stk.top() != cur) {
            component.push_back(stk.top());
            stk.pop();
        }
        component.push_back(stk.top());
        stk.pop();
        ans.emplace_back(std::move(component));
    }
}

void Kosaraju::get_component(int cur, ATGraph &G, std::vector<int> &comp) {
    visited[cur] = true;
    comp.push_back(cur);
    for (auto node: G.adjTab[cur]) {
        if (!visited[node]) {
            get_component(node, G, comp);
        }
    }
}

void Kosaraju::get_order(int cur, ATGraph &G) {
    visited[cur] = true;
    for (auto node: G.adjTab[cur]) {
        if (!visited[cur]) {
            get_order(node, G);
        }
    }
    order.push(cur);
}

ATGraph Kosaraju::reverse_graph(ATGraph &G) {
    ATGraph ans(G.n);
    for (int i = 0; i < G.n; ++i) {
        for (auto node: G.adjTab[i]) {
            ans.add_edge(node, i);
        }
    }
    return ans;
}

std::vector<std::vector<int>> Kosaraju::find(ATGraph &G) {
    auto G_ = reverse_graph(G);
    visited.resize(G.n, false);
    for (int i = 0; i < G_.n; ++i) {
        if (!visited[i]) {
            get_order(i, G_);
        }
    }
    for (auto &&v: visited) {
        v = false;
    }
    std::vector<std::vector<int>> ans;

    while (!order.empty()) {
        int cur = order.top();
        order.pop();
        if (!visited[cur]) {
            ans.emplace_back();
            get_component(cur, G, ans.back());
        }
    }
    visited.clear();
    return ans;
}