//
// Created by Amadeus on 2023/8/9.
//

#ifndef ALOGRITHMC_PARALLEL_H
#define ALOGRITHMC_PARALLEL_H

#include <thread>

// 信号量
class Semaphore {
private:
    std::mutex mutex_;
    std::condition_variable cv_;
    int count_;
public:
    explicit Semaphore(int count = 0) : count_(count) {}

    inline void Set(int count) { count_ = count; }

    inline void Signal() {
        std::unique_lock<std::mutex> lock(mutex_);
        ++count_;
        cv_.notify_one();
    }

    inline void Wait() {
        std::unique_lock<std::mutex> lock(mutex_);
        cv_.wait(lock, [this] { return count_ > 0; });
        --count_;
    }
};

#endif //ALOGRITHMC_PARALLEL_H
