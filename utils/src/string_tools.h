//
// Created by Amadeus on 2022/9/13.
//

#ifndef EULER_PROJECT_CPP_STRING_TOOLS_H
#define EULER_PROJECT_CPP_STRING_TOOLS_H

#include <string>
#include <vector>

// trim from start (in place)
static inline void ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](unsigned char ch) {
        return !std::isspace(ch);
    }));
}

// trim from end (in place)
static inline void rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(), [](unsigned char ch) {
        return !std::isspace(ch);
    }).base(), s.end());
}

// trim from both ends (in place)
static inline void trim(std::string &s) {
    ltrim(s);
    rtrim(s);
}

// split a string to sub-strings (in place)
static inline std::vector<std::string> split(std::string &s, const std::string &delimiter) {
    std::vector<std::string> res;
    size_t pos;
    std::string token;
    while ((pos = s.find(delimiter)) != std::string::npos) {
        res.push_back(s.substr(0, pos));
        s.erase(0, pos + delimiter.length());
    }
    res.push_back(s);
    return res;
}

static inline std::vector<std::string> split(std::string &s, char delimiter) {
    std::vector<std::string> res;
    size_t pos;
    while ((pos = s.find(delimiter)) != std::string::npos) {
        res.push_back(s.substr(0, pos));
        s.erase(0, pos + 1);
    }
    res.push_back(s);
    return res;
}

#endif //EULER_PROJECT_CPP_STRING_TOOLS_H
