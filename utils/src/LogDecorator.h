//
// Created by Amadeus on 2022/9/22.
//

#ifndef ALOGRITHMC_LOGDECORATOR_H
#define ALOGRITHMC_LOGDECORATOR_H

#include <iostream>

class LogDecorator {
private:
    std::ostream &os_;
public:
    explicit LogDecorator(std::ostream &os) : os_(os) {}

    template<typename Arg, typename... Args>
    void doPrint(std::ostream &out, Arg &&arg, Args &&... args) const {
        out << std::forward<Arg>(arg);
        using expander = int[];
        (void) expander{0, (void(out << ',' << std::forward<Args>(args)), 0)...};
    }

    template<typename F, typename... A>
    auto operator()(F f, A ... a) const -> // -> 是指定返回值的意思，后面这些是模版的偏特化
    typename std::enable_if<
            not std::is_same<
                    typename std::result_of<F(A...)>::type,
                    void>::value,
            typename std::result_of<F(A...)>::type>::type {
        os_ << "invoke " << f << std::endl;
        doPrint(os_, std::forward<A &&>(a)...);
        auto &&result = f(std::forward<A &&>(a)...);
        os_ << "return " << std::endl;
        return std::move(result);
    }

    template<typename F, typename... A>
    auto operator()(F f, A ... a) const ->
    typename std::enable_if<
            std::is_same<typename std::result_of<F(A...)>::type,
                    void>::value,
            typename std::result_of<F(A...)>::type>::type {
        os_ << "invoke " << f << std::endl;
        doPrint(os_, std::forward<A &&>(a)...);
        f(std::forward<A &&>(a)...);
        os_ << "return " << std::endl;
        return;
    }
};

#endif //ALOGRITHMC_LOGDECORATOR_H
