//
// Created by Amadeus on 2022/9/22.
//

#ifndef ALOGRITHMC_CACHEDECORATOR_H
#define ALOGRITHMC_CACHEDECORATOR_H

template<typename R, typename... A>
class CacheDecorator {
public:
    /* f: R(A...)
     *    And if A contains array type, you should use std::vector instead.
     *    And if A contains custom class, you should implement operator< and operator== for it.
     * N.B. Beware of the lifetime of the function object
     */

    explicit CacheDecorator(std::function<R(A &&...)> f) : f_(f) {}


    R operator()(A... a) {
        std::tuple<typename std::decay<A>::type...> key(a...);
        auto search = map_.find(key);
        if (search != map_.end()) {
            return search->second;
        }
        auto res = map_.emplace(std::move(key),
                                f_(std::forward<A &&>(a)...));
        return res.first->second;
    };

private:
    std::function<R(A &&...)> f_;
    std::map<std::tuple<typename std::decay<A>::type...>, R> map_;
};

#endif //ALOGRITHMC_CACHEDECORATOR_H
