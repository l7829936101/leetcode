//
// Created by Amadeus on 2022/9/13.
//

#ifndef EULER_PROJECT_CPP_INTVECTORPARSER_H
#define EULER_PROJECT_CPP_INTVECTORPARSER_H

#include <string>

#include <vector>

class IntVectorParser {
public:
    static std::vector<int> parse(std::string &&str);
};


#endif //EULER_PROJECT_CPP_INTVECTORPARSER_H
