//
// Created by Amadeus on 2022/9/14.
//

#include "BinaryTreeParser.h"

#include <queue>
#include "util.h"

static inline TreeNode *ston(const std::string &str) {
    if (str == "null") {
        return nullptr;
    } else {
        return new TreeNode(std::stoi(str));
    }
}

TreeNode *BinaryTreeParser::parse(std::string &&str) {
    trim(str);
    //TODO 改成 ParseError
    assert('[' == str.front() && ']' == str.back());

    str = str.substr(1, str.size() - 2);
    if (str.empty()) {
        return nullptr;
    }

    auto strs = split(str, ',');
    for (auto s: strs) {
        trim(s);
    }

    auto root = ::ston(strs[0]);
    std::queue<TreeNode *> nodes;
    nodes.push(root);

    int i = 1;
    int n = int(strs.size());
    TreeNode *node, *sub_node;
    while (!nodes.empty()) {
        node = nodes.front();
        nodes.pop();
        if (node != nullptr) {
            if (i >= n) {
                break;
            }
            sub_node = ston(strs[i]);
            node->left = sub_node;
            ++i;
            nodes.push(sub_node);
            if (i >= n) {
                break;
            }
            sub_node = ston(strs[i]);
            node->right = sub_node;
            ++i;
            nodes.push(sub_node);
        }
    }
    return root;
}
