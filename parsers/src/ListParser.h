//
// Created by Amadeus on 2022/9/14.
//

#ifndef EULER_PROJECT_CPP_LISTPARSER_H
#define EULER_PROJECT_CPP_LISTPARSER_H

#include "struct.h"

#include <string>

class ListParser {
public:
    static ListNode *parse(std::string &&str);
};


#endif //EULER_PROJECT_CPP_LISTPARSER_H
