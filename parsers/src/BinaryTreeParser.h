//
// Created by Amadeus on 2022/9/14.
//

#ifndef EULER_PROJECT_CPP_BINARYTREEPARSER_H
#define EULER_PROJECT_CPP_BINARYTREEPARSER_H

#include <string>
#include "struct.h"

class BinaryTreeParser {
public:
    static TreeNode *parse(std::string &&str);
};


#endif //EULER_PROJECT_CPP_BINARYTREEPARSER_H
