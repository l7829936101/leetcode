//
// Created by Amadeus on 2022/9/14.
//

#include "ListParser.h"
#include "util.h"

ListNode *ListParser::parse(std::string &&str) {
    std::vector<int> res;
    trim(str);

    //TODO 改成 ParseError
    assert('[' == str.front() && ']' == str.back());

    str = str.substr(1, str.size() - 2);
    auto strs = split(str, ',');
    if (strs.empty()) {
        return nullptr;
    }
    trim(strs[0]);
    auto temp = new ListNode(std::stoi(strs[0]));
    ListNode *head = temp;
    int n = int(strs.size());
    for (int i = 1; i < n; ++i) {
        trim(strs[i]);
        temp->next = new ListNode(std::stoi(strs[i]));
        temp = temp->next;
    }
    return head;
}
