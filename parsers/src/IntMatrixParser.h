//
// Created by Amadeus on 2022/9/13.
//

#ifndef EULER_PROJECT_CPP_INTMATRIXPARSER_H
#define EULER_PROJECT_CPP_INTMATRIXPARSER_H

#include <vector>
#include <string>

class IntMatrixParser {
public:
    static std::vector<std::vector<int>> parse(std::string &&str);
};


#endif //EULER_PROJECT_CPP_INTMATRIXPARSER_H
