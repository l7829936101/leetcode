//
// Created by Amadeus on 2022/9/13.
//

#include "IntVectorParser.h"

#include "util.h"

std::vector<int> IntVectorParser::parse(std::string &&str) {
    std::vector<int> res;
    trim(str);

    //TODO 改成 ParseError
    assert('[' == str.front() && ']' == str.back());

    str = str.substr(1, str.size() - 2);
    auto strs = split(str, ',');

    for (auto &s: strs) {
        trim(s);
        int temp = std::stoi(s);
        res.push_back(temp);
    }

    return res;
}