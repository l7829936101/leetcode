//
// Created by Amadeus on 2022/9/13.
//

#include "IntMatrixParser.h"

#include "IntVectorParser.h"

#include "util.h"

std::vector<std::vector<int>> IntMatrixParser::parse(std::string &&str) {
    trim(str);

    //TODO 改成 ParseError
    assert('[' == str.front() && ']' == str.back());

    str = str.substr(1, str.size() - 2);
    auto strs = split(str, "],");
    std::vector<std::vector<int>> res;
    res.reserve(strs.size());
    for (auto &s: strs) {
        res.push_back(IntVectorParser::parse(s + ']'));
    }
    return res;
}
